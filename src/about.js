import React from "react";
// class About extends React.Component {
//     render = () => (
//         <div className="container mt-2">
//             <h1>About</h1>
//         </div>
//     );
// }
export default class About extends React.Component {
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         email: "",
    //         pass: ""
    //     }

    //     this.mengisiEmail = this.mengisiEmail.bind(this);
    //     this.mengisiPassword = this.mengisiPassword.bind(this);
    //     this.handleSubmit = this.handleSubmit.bind(this);
    // }

    // mengisiEmail(event) { this.setState({ email: event.target.value }); }
    // mengisiPassword(event) { this.setState({ pass: event.target.value }); }
    // handleSubmit(event) {
    //     this.state.email == 'test@email.com' && this.state.pass == '12345' ?
    //         alert('Login Sukses') : alert('Login gagal, coba isi dengan email: test@email.com & password: 12345 ')
    // }

    // render() {
    //     return (
    //         <div className="test">
    //             <form onSubmit={this.handleSubmit}>
    //                 <label>Email :
    //             <input type="text" onChange={this.mengisiEmail} />
    //                 </label><br />
    //                 <label>Password :
    //             <textarea onChange={this.mengisiPassword} />
    //                 </label>
    //                 <input type="submit" value="Submit" />
    //                 <input type="reset" value="Reset" />
    //             </form>
    //         </div>

    //     );
    // }

    // register
    constructor(props) {
        super(props);
        this.state = {
            nama: '',
            email: '',
            pass: '',
            pass2: '',
            pekerjaan: ''
        }

        this.menyimpanNama = this.menyimpanNama.bind(this);
        this.menyimpanEmail = this.menyimpanEmail.bind(this);
        this.menyimpanPassword = this.menyimpanPassword.bind(this);
        this.menyimpanPassword2 = this.menyimpanPassword2.bind(this);
        this.menyimpanPekerjaan = this.menyimpanPekerjaan.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    menyimpanNama(event) { this.setState({ nama: event.target.value }); }
    menyimpanEmail(event) { this.setState({ email: event.target.value }); }
    menyimpanPassword(event) { this.setState({ pass: event.target.value }); }
    menyimpanPassword2(event) { this.setState({ pass2: event.target.value }); }

    menyimpanPekerjaan(event) { this.setState({ pekerjaan: event.target.value }); }
    handleSubmit(event) {
        if (this.state.nama == '') {
            alert('Nama tidak boleh kosong!');
        } else if (this.state.email == '') {
            alert('Email tidak boleh kosong!');
        } else if (this.state.pass == '') {
            alert('Password tidak boleh kosong!');
        } else if (this.state.pass != this.state.pass2) {
            alert('Password tidak sama!');
        } else {
            alert('nama adalah : ' + this.state.nama + ', email anda : ' + this.state.email + ', password anda : ' + this.state.pass + ', dan pekerjaan anda adalah ' + this.state.pekerjaan);
        }
    }

    render() {
        return (
            <div className="App-header">
                <form onSubmit={this.handleSubmit}>
                    <table>
                        <tr>
                            <td><label>Nama </label></td>
                            <td><input type="text" onChange={this.menyimpanNama} /></td>
                        </tr>
                        <tr>
                            <td><label>Email </label></td>
                            <td><input type="email" onChange={this.menyimpanEmail} /></td>
                        </tr>
                        <tr>
                            <td><label>Password </label></td>
                            <td><input type="password" onChange={this.menyimpanPassword} /></td>
                        </tr>
                        <tr>
                            <td><label>Ulangi Password </label></td>
                            <td><input type="password" onChange={this.menyimpanPassword2} /></td>
                        </tr>
                        <tr>
                            <td><label>Pekerjaan x</label></td>
                            <td>
                                <select value={this.state.pekerjaan} onChange={this.menyimpanPekerjaan}>
                                    <option value="Fullstack Developer">Fullstack Developer</option>
                                    <option value="Frontend Developer">Frontend Developer</option>
                                    <option value="Backend Developer">Backend Developer</option>
                                    <option value="UI/UX Designer">UI/UX Designer</option>
                                </select>
                            </td>
                        </tr>
                        <tr><td></td>
                            <td><input type="submit" value="Submit" />
                            <input type="reset" value="Reset" /></td>
                        </tr>
                    </table>
                </form>

            </div>
        );
    }

}
