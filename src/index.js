import React from 'react';
import ReactDOM from 'react-dom';
//import './index.css';
import App from './App';
//import './App.css';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import './components/asset/stail.css';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';
import About from './about';
import { Button } from 'reactstrap';

//ini yang pertama
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// routing react
// const routing = (
//   <Router>
//     <nav className="navbar navbar-expand-lg navbar-light bg-light">
//       <button
//         className="navbar-toggler"
//         type="button"
//         data-toggle="collapse"
//         data-target="#navbarSupportedContent"
//         aria-controls="navbarSupportedContent"
//         aria-expanded="false"
//         aria-label="Toggle navigation">
//           <span className="navbar-toggler-icon"></span>
//       </button>
//       <div className="collapse navbar-collapse" i
//         d="navbarSupportedContent">
//         <ul className="navbar-nav mr-auto">
//           <li className="nav-item active">
//             <Link className="navbar-brand" to="/">
//               Login
//   </Link>
//           </li>
//           <li className="nav-item">
//             <Link className="navbar-brand" to="/about">
//               Register
//   </Link>
//           </li>
//         </ul>
//       </div>
//     </nav>
//     <Link>
//     </Link>
//     {/* <Link to="/about"><button>Register</button></Link>
//     <Link to="/"><button>Login</button></Link> */}
//     <Route exact path="/" component={App} />
//     <Route path="/about" component={About} />
//   </Router>
// );
// ReactDOM.render(routing, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
