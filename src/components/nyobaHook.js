import React, { useState } from 'react';
import { Button } from 'reactstrap';

//pakai hook
function NyobaHook() {
    const [count, test] = useState('Ujang');

    return (
        <div>
            <p>it's {count} time</p>
            <Button onClick={() => { count === 'Ujang' ? test('Meredith') : test('Ujang'); }} >
                Ubah Namae
                 </Button>
        </div>
    );
}

export default NyobaHook