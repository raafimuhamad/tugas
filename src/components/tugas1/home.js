import React from 'react';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';
import Register from './register';
import Login from './login';
import './asset/style.css';


// routing react
const home = props => {
    return (
        <Router>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark rig">
                <button
                    className="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse"
                    id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <Link className="navbar-brand" to="/">Login</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="navbar-brand" to="/register">Register</Link>
                        </li>
                    </ul>
                </div>
            </nav>
            <Route exact path="/" component={Login} />
            <Route path="/register" component={Register} />
        </Router>
    )
}


export default home

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls
// Learn more about service workers: https://bit.ly/CRA-PWA