import React, { useState, useEffect } from "react";

// ini component
// export default class MyComponent extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       name: "John"
//     };
//     this.handleNameChange = this.handleNameChange.bind(this);
//   }

//   componentDidMount() {
//     document.title = this.state.name;
//   }

//   componentDidUpdate() {
//     document.title = this.state.name;
//   }

//   handleNameChange(e) {
//     this.setState({
//       name: e.target.value
//     });
//   }

//   render() {
//     return (
//       <section>
//         <input 
//           value={this.state.name} 
//           onChange={this.handleNameChange} 
//         />
//       </section>
//     );
//   }
// }

// ini Hook
export default function MyComponent(props) {
    const [name, setName] = useState("Seth");
    
    useEffect(() => {
      document.title = name;
    });
  
    function handleNameChange(e) {
      setName(e.target.value);
    }
  
    return (
      <section>
        <input
          value={name}
          onChange={handleNameChange}
        />
      </section>
    );
  }